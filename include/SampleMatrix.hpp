#pragma once

#include <array>
#include <map>

#include "Vector2.hpp"

const size_t NUM_ROWS = 5;
const size_t NUM_COLS = 7;
const size_t NUM_SAMPLES = NUM_COLS * NUM_ROWS;

using VectorUV = Vector2;

class SampleMatrix {
public:
    SampleMatrix() {;}
    SampleMatrix(const std::array<Vector2, NUM_SAMPLES> samples) : samples(samples) {;}
    
    double calculate_metric_A() const;
    double calculate_metric_B() const;
    double calculate_adjacent_difference(const size_t row, const size_t col) const;

    const Vector2& operator [] (const size_t i) const;
    Vector2& operator [] (const size_t i);
private:
    std::array<Vector2, NUM_SAMPLES> samples;
};