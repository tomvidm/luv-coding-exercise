#pragma once

#include <cmath>

struct Vector2 {
    Vector2() : x(0), y(0) {;}
    Vector2(const double x, const double y) : x(x), y(y) {;}
    Vector2(const Vector2& other) : x(other.x), y(other.y) {;}

    Vector2 operator = (const Vector2 other);

    double x;
    double y;
};

Vector2 operator + (const Vector2 lhs, const Vector2 rhs);
Vector2 operator - (const Vector2 lhs, const Vector2 rhs);
Vector2 operator * (const double lhs, const Vector2 rhs);

double magnitudeSquared(const Vector2 vec);
double magnitude(const Vector2 vec);
