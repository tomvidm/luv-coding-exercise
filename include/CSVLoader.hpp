#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <vector>

typedef std::vector<std::string> CSVRow;
typedef std::vector<CSVRow> CSVTable;

class CSVLoader {
public:
    static CSVTable loadFromFile(const std::string path);
};

std::vector<std::string> split(const std::string str, const char delimiter);