#include <string>

// This should not really be a template, but I wanted to
// separate the number of columns from the specific row-column
// pair from which the index is calculated.
template <size_t COLUMNS>
size_t row_col_to_index(const size_t row, const size_t col) {
    return col + row * COLUMNS;
}
