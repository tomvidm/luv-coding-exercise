## The data format
The following can be said about the data formatting in the provided .csv file
* Ignore rows 0, 1 and 2
* For all rows `R` >= 3:
  * Column 0 holds the index of the sample, starting with 1
  * Columns 1, 4, 7, 10, 13, 16, 19 can be ignored
  * All other columns holds a floating point number

# In short words, what needs to be done
Thirty five xy-vectors are arranged in a 5x7 grid. Two metrics shall be computed:
* The distance between any pairs of vectors A and B for which the distance is maximized.
* The distance between any pairs of horizontally or vertically adjacent vectors A and B for which the distance is maximized.