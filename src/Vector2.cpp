#include "Vector2.hpp"

Vector2 Vector2::operator = (const Vector2 other) {
    x = other.x;
    y = other.y;
    return *this;
}

Vector2 operator + (const Vector2 lhs, const Vector2 rhs) {
    return Vector2(lhs.x + rhs.x, lhs.y + rhs.y);
}

Vector2 operator - (const Vector2 lhs, const Vector2 rhs) {
    return Vector2(lhs.x - rhs.x, lhs.y - rhs.y);
}

Vector2 operator * (const double lhs, const Vector2 rhs) {
    return Vector2(lhs * rhs.x, lhs * rhs.y);
}

double magnitudeSquared(const Vector2 vec) {
    return vec.x * vec.x + vec.y * vec.y;
}

double magnitude(const Vector2 vec) {
    return sqrt(magnitudeSquared(vec));
}