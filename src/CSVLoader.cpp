#include "CSVLoader.hpp"

CSVTable CSVLoader::loadFromFile(const std::string path) {
    std::ifstream file(path);

    CSVRow lines;
    std::string line;

    while (getline(file, line)) { lines.push_back(line); }
    file.close();

    CSVTable result;

    for (auto& line : lines) {
        result.push_back(split(line, ','));
    }

    return result;
} 

CSVRow split(const std::string str, const char delimiter) {
    CSVRow result;

    std::stringstream ss(str);
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, delimiter);
        result.push_back(substr);
    }

    return result;
}