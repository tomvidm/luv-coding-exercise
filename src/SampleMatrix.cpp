#include <stdexcept>

#include "SampleMatrix.hpp"
#include "Helpers.hpp"

double SampleMatrix::calculate_metric_A() const {
    double maxDifference = 0.0;
    for (size_t i = 0; i < NUM_SAMPLES; i++) {
        for (size_t j = i + 1; j < NUM_SAMPLES; j++) {
            const Vector2 luv_i = samples[i];
            const Vector2 luv_j = samples[j];
            const double difference = magnitude(luv_i - luv_j);
            if (difference > maxDifference) {
                maxDifference = difference;
            }
        }
    }

    return maxDifference;
}

double SampleMatrix::calculate_metric_B() const {
    double maxDifference = 0.0;
    for (size_t r = 0; r < NUM_ROWS; r++) {
        for (size_t c = 0; c < NUM_COLS; c++) {
            const double diff_here = calculate_adjacent_difference(r, c);
            if (diff_here > maxDifference) {
                maxDifference = diff_here;
            }
        }
    }

    return maxDifference;
}

// Calculating for each "cross". This means recalculating most of the pairs
// twice, but it is still O(N), and 35 samples is not much. A different solution would be
// to store differences in a key-value map, where the keys are the index-pairs. But the only
// difference would be to approximately halve the amount of calculations. But that would also
// mean a 35x35 triangular matrix.
double SampleMatrix::calculate_adjacent_difference(const size_t row, const size_t col) const {
    const size_t index_here =   row_col_to_index<NUM_COLS>(row, col);
    const size_t index_up =     row_col_to_index<NUM_COLS>(row + 1, col);
    const size_t index_down =   row_col_to_index<NUM_COLS>(row - 1, col);
    const size_t index_left =   row_col_to_index<NUM_COLS>(row, col - 1);
    const size_t index_right =  row_col_to_index<NUM_COLS>(row, col + 1);

    double maxDifference = 0.0;
    const Vector2 luv_here = samples[index_here];

    if (index_up < NUM_SAMPLES) {
        const Vector2 luv_up = samples[index_up];
        const double diff = magnitude(luv_here - luv_up);
        if (diff > maxDifference) { maxDifference = diff; }
    }
    if (index_down < NUM_SAMPLES) {
        const Vector2 luv_down = samples[index_down];
        const double diff = magnitude(luv_here - luv_down);
        if (diff > maxDifference) { maxDifference = diff; }
    }
    if (index_left < NUM_SAMPLES) {
        const Vector2 luv_left = samples[index_left];
        const double diff = magnitude(luv_here - luv_left);
        if (diff > maxDifference) { maxDifference = diff; }
    }
    if (index_right < NUM_SAMPLES) {
        const Vector2 luv_right = samples[index_right];
        const double diff = magnitude(luv_here - luv_right);
        if (diff > maxDifference) { maxDifference = diff; }
    }

    return maxDifference;
}

const Vector2& SampleMatrix::operator [] (const size_t i) const {
    return samples[i];
}

Vector2& SampleMatrix::operator [] (const size_t i) {
    return samples[i];
}