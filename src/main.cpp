#include <iostream>
#include <iomanip>
#include <array>
#include <stdexcept>

#include "Vector2.hpp"
#include "SampleMatrix.hpp"
#include "CSVLoader.hpp"

const size_t NUM_MEASUREMENTS = 8;
const size_t SAMPLES_PER_MEASUREMENT = 35;

int main() {
    CSVTable table = CSVLoader::loadFromFile("../resources/samples.csv");
    
    std::array<std::array<Vector2, SAMPLES_PER_MEASUREMENT>, NUM_MEASUREMENTS> measurements;
    for (size_t measid = 0; measid < NUM_MEASUREMENTS; measid++) {
        for (size_t sampleid = 0; sampleid < SAMPLES_PER_MEASUREMENT; sampleid++) {
            try {
                Vector2 luv(
                    // These magnic numbers just ensures that the empty columns are ignored
                    std::stod(table[3 + sampleid][2 + 3 * measid]),
                    std::stod(table[3 + sampleid][2 + 3 * measid + 1])
                );
                measurements[measid][sampleid] = luv;
            } catch (std::invalid_argument& inv) {
                std::cerr << inv.what() << std::endl;
            }
        }
    }

    std::cout << "           Metric A     Metric B" << std::endl;

    // Sample 1 - White
    SampleMatrix s1white(measurements[0]);
    std::cout << "S1 White - " 
              << s1white.calculate_metric_A() << "\t"
              << s1white.calculate_metric_B() << std::endl;
    // Sample 1 - Red
    SampleMatrix s1red(measurements[1]);
    std::cout << "S1 Red   - "
              << s1red.calculate_metric_A() << "\t"
              << s1red.calculate_metric_B() << std::endl;

    // Sample 1 - Green
    SampleMatrix s1green(measurements[2]);
    std::cout << "S1 Green - " 
              << s1green.calculate_metric_A() << "\t"
              << s1green.calculate_metric_B() << std::endl;

    // Sample 1 - Blue
    SampleMatrix s1blue(measurements[3]);
    std::cout << "S1 Blue  - " 
              << s1blue.calculate_metric_A() << "\t"
              << s1blue.calculate_metric_B() << std::endl;

    // Sample 2 - White
    SampleMatrix s2white(measurements[4]);
    std::cout << "S2 White - " 
              << s2white.calculate_metric_A() << "\t"
              << s2white.calculate_metric_B() << std::endl;

    // Sample 2 - Red
    SampleMatrix s2red(measurements[5]);
    std::cout << "S2 Red   - " 
              << s2red.calculate_metric_A() << "\t"
              << s2red.calculate_metric_B() << std::endl;
    // Sample 2 - Green
    SampleMatrix s2green(measurements[6]);
    std::cout << "S2 Green - " 
              << s2green.calculate_metric_A() << "\t"
              << s2green.calculate_metric_B() << std::endl;

    // Sample 2 - Blue
    SampleMatrix s2blue(measurements[7]);
    std::cout << "S2 Blue  - " 
              << s2blue.calculate_metric_A() << "\t"
              << s2blue.calculate_metric_B() << std::endl;

    return 0;
}