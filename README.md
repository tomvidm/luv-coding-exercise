# Compile and run
The path to the .csv file is hardcoded, so the luv executable should be run woth build/ as the working directory.
## Ubuntu / Linux / etc
```
mkdir build
cd build
cmake .. && make && ./luv
```

## Windows (w/ Git Bash)
```
mkdir build
cd build
cmake ..
```

The compile using whatever IDE you have and run the executable. I repeat: build/ should be the working directory.

# Expected result
```
           Metric A     Metric B
S1 White - 0.114872	0.0477481
S1 Red   - 0.105784	0.0392546
S1 Green - 0.0265082	0.0160387
S1 Blue  - 0.0514547	0.0265332
S2 White - 0.146251	0.0570642
S2 Red   - 0.118025	0.0503935
S2 Green - 0.0279276	0.0158477
S2 Blue  - 0.0303338	0.00877276

```

# Reasoning during the work
* CSVTable is just a typedef of a nested std::vector, containing strings. This gives me an easy way of accessing the string fields. A custom class could be written, but the std::vector interface provides the methods needed already, so a typedef is more than
* The u' and v' components are simply stored in a simple two dimensional vector class. For the purpose of this task, they are functionally identical, as the third dimension is not considered here. Can be typedef'd for readability.
* CSVLoader is simply a class with one static method. I see no reason to store a specific dataset in something called a Loader. I could choose to just make a function loadCSVTable, without any class, but a situation can be imagined where a CSVLoader will load from different sources, and keeping them organized under the name CSVLoader contribute to readability.
* The SampleMatrix stores 35 elements in a one dimensional std::array. An alternative would be to store it in a nested array. Either case, only one of the calculation methods would be simplified. In this case, comparing every pair of u'v' pairs is simplest with the current implementation, while requiring more intermediate calculation of indexes for the second method. (Finding the maximum distance between any two adjacent u'v' samples)
* The were no assumptions about the user in the task, so main() is simply hardcoded to output the correct result. The path to the csv file is also hardcoded relative to the build/ directory, so the program is absolutely not user friendly.